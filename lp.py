import numpy
from   functools      import reduce
from   numbers        import Number
from   scipy.optimize import linprog
from   tupledict      import *
from   util           import *


LESS_EQUAL    = 0
GREATER_EQUAL = 1
EQUAL         = 2


def quicksum(var_list):
    return (
        LinExpr.build_from_terms(Term(1.0, var_list[0]))
            if   len(var_list) == 1
            else reduce(lambda x, y: x + y, var_list))


class Var:

    # Constructors #

    def __init__(self, col, lb, ub, obj, name):
        self.__col  = col
        self.__bnd  = (lb, ub)
        self.__obj  = obj
        self.__name = name
        self.__x    = None

    # Operators #

    def __neg__(self):
        return Term(-1.0, self)

    def __add__(self, other):
        return Term(1.0, self) + other

    def __sub__(self, other):
        return Term(1.0, self) - other

    def __mul__(self, val):
        return Term(float(val), self)

    __rmul__ = __mul__

    # Getters / Attributes #

    def column(self):
        return self.__col

    def bounds(self):
        return self.__bnd

    def set_obj(self, obj):
        self.__obj = obj

    def obj(self):
        return self.__obj

    def name(self):
        return self.__name

    def x(self):
        return self.__x

    # Semi-private #

    def set_x(self, x):
        self.__x = x


class Term:

    # Constructors #

    def __init__(self, cof, var):
        self.__cof = cof
        self.__var = var

    # Operators #

    def __add__(self, other):
        return LinExpr.build_from_terms(self, other)

    def __sub__(self, other):
        if isinstance(other, Var):
            other = Term(1.0, other)
        return LinExpr.build_from_terms(self, other.flip_sign())

    # Getters / Attributes #

    def cof(self):
        return self.__cof

    def var(self):
        return self.__var

    # Semi-private #

    def flip_sign(self):
        self.__cof *= -1.0
        return self


class LinExpr:

    # Constructors #

    def __init__(self, arg1=None, arg2=None):
        assert (
            (isinstance(arg1, list)    and arg2 is None)          or
             isinstance(arg1, list)    and isinstance(arg2, list) or
             isinstance(arg1, LinExpr) and arg2 is None           or
             arg1 is None              and arg2 is None
          )
        if isinstance(arg1, list) and isinstance(arg2, list):
            assert len(arg1) == len(arg2), "arg1 and arg2 are unequal lengths"
            return LinExpr(zip(arg1, arg2))
        elif isinstance(arg1, list):
            self.__terms = [Term(c,v) for c, v in arg1]
            self.__op    = None
            self.__rhs   = 0.0
        elif isinstance(arg1, LinExpr):
            self.__terms = list(arg1.__terms)
            self.__op    = arg1.__op
            self.__rhs   = arg1.__rhs
        else:
            self.__terms = []
            self.__op    = None
            self.__rhs   = 0.0

    def copy(other):
        return LinExpr(other)

    # Builders #

    @classmethod
    def build_from_terms(self, *args):
        expr = LinExpr()
        for arg in args:
            expr += arg
        return expr

    # Public #

    def addTerms(self, cof, var):
        both_arr    = isinstance(cof, list)   and isinstance(var, list)
        both_atomic = isinstance(cof, Number) and isinstance(var, Var)
        assert both_arr or both_atomic, "cof and var are unlike types"
        assert not both_arr or len(cof) == len(var), "cof and var unequal sizes"
        if both_arr and len(cof) == 0:
            return
        cof = listify(cof)
        var = listify(var)
        for c, v in zip(cof, var):
            self.__terms.append(Term(c, v))

    # Operators #

    def __add__(self, term):
        if   isinstance(term, Var):
            self.__terms.append(Term(1.0, term))
        elif isinstance(term, Term):
            self.__terms.append(term)
        elif isinstance(term, Number):
            self.__rhs -= term
        else:
            raise TypeError('Invalid term added')
        return self

    def __sub__(self, term):
        if   isinstance(term, Var):
            self.__terms.append(Term(-1.0, term))
        elif isinstance(term, Term):
            self.__terms.append(term.flip_sign())
        elif isinstance(term, Number):
            self.__rhs += term
        else:
            raise TypeError('Invalid term subtracted')
        return self

    def __le__(self, rhs):
        self.__op = LESS_EQUAL
        self.set_rhs(rhs)
        return self

    def __eq__(self, rhs):
        self.__op  = EQUAL
        self.__rhs = rhs
        return self

    def __ge__(self, rhs):
        self.__op = GREATER_EQUAL
        self.set_rhs(rhs)
        return self

    # Getters / Attributes #

    def terms(self):
        return self.__terms

    def size(self):
        return len(self.__terms)

    def op(self):
        return self.__op

    def rhs(self):
        return self.__rhs

    def has_sense(self):
        return self.__op != None

    # Semi-private #

    def set_sense(self, op):
        self.__op = op

    def set_rhs(self, rhs):
        if   isinstance(rhs, Number):
            self.__rhs += rhs
        elif isinstance(rhs, Term):
            self.__terms.append(rhs.flip_sign())
        elif isinstance(rhs, Var):
            self.terms.append(Term(-1.0, rhs))
        else:
            raise TypeError('Invalid rhs type')

    def flip_sense(self):
        assert self.__op is not None, 'flip() called before setting sense'
        for term in self.__terms:
            term.flip_sign()
        if self.__op != EQUAL:
            self.__rhs *= -1.0
            self.__op  ^= 1


class Model:

    # Constructors #

    def __init__(self):
        self.__vars = []
        self.__A_ub = []
        self.__b_ub = []
        self.__A_eq = []
        self.__b_eq = []

    # Public #

    # Note: vtype is ignored
    def addVar(self, lb=None, ub=None, obj=0.0, vtype=None, name=""):
        new_var = Var(len(self.__vars), lb, ub, obj, name)
        self.__vars.append(new_var)
        return new_var

    # indices can be any of the following:
    # 1) A number
    # 2) A list or set of numbers
    # 3) A tuple
    # 4) A list of tuples
    # Note: vtype is ignored
    def addVars(self, indices, lb=None, ub=None, obj=0.0, vtype=None, name=""):
        indices = tuplelist(indices)
        n       = len(indices)
        s       = len(self.__vars)
        lb      = listify(lb, n)
        ub      = listify(ub, n)
        obj     = listify(obj, n)
        if not isinstance(name, list):
            name = map(lambda idx: name + str(idx), indices)
        new_vars = [ Var(_col, _lb, _ub, _obj, _name)
                     for (_col, _lb, _ub, _obj, _name)
                     in zip(range(s, s+n), lb, ub, obj, name) ]
        self.__vars += new_vars
        return tupledict().add(indices, new_vars)

    def setObjective(self, expr):
        for term in expr.terms():
            term.var().set_obj(term.cof())

    def addConstr(self, *args):
        expr = args[0]
        if isinstance(expr, Var):
            expr = LinExpr.build_from_terms(Term(1.0, expr))
        assert isinstance(expr, LinExpr)
        if not expr.has_sense():
            expr.set_sense(args[1])
            expr.set_rhs(args[2])
        if expr.op() == GREATER_EQUAL:
            expr.flip_sense()
        row = [0.0] * len(self.__vars)
        for term in expr.terms():
            row[term.var().column()] += term.cof()
        if expr.op() == EQUAL:
            self.__A_eq.append(row)
            self.__b_eq.append(expr.rhs())
        else:
            self.__A_ub.append(row)
            self.__b_ub.append(expr.rhs())

    def resize_matrices(self):
        self.__mat_resize(self.__A_ub)
        self.__mat_resize(self.__A_eq)

    # only supports linear programs
    def optimize(self):
        self.resize_matrices()
        res = linprog(
                c      = list(map(lambda v: v.obj(), self.__vars))      ,
                A_ub   = None if len(self.__A_ub) == 0 else self.__A_ub ,
                b_ub   = self.__b_ub                                    ,
                A_eq   = None if len(self.__A_eq) == 0 else self.__A_eq ,
                b_eq   = self.__b_eq                                    ,
                bounds = list(map(lambda v: v.bounds(), self.__vars))   ,
            )
        if res.success:
            for i,x in enumerate(res.x):
                self.__vars[i].set_x(x)
        return res

    # Getters / Attributes #

    def getVars(self):
        return self.__vars

    def A_ub(self):
        return self.__A_ub

    def b_ub(self):
        return self.__b_ub

    def A_eq(self):
        return self.__A_eq

    def b_eq(self):
        return self.__b_eq

    # Private #

    def __mat_resize(self, A):
        n = len(self.__vars)
        for row in A:
            dim_diff = n - len(row)
            if dim_diff == 0:
                continue
            row += [[0.0]] * dim_diff


# Test example and scipy.optimize.linprog documentation:
# https://docs.scipy.org/doc/scipy-0.19.1/reference/generated/scipy.optimize.linprog.html

# The true value of this API is not the syntactical sugar seen
# below, rather being able to reference variables with any
# custom key type.

if __name__ == '__main__':

    mdl = Model()
    x0  = mdl.addVar()
    x1  = mdl.addVar(lb=-3, ub=None)

    mdl.setObjective(-x0 + 4 * x1)

    mdl.addConstr(-3 * x0 + x1 <= 6)
    mdl.addConstr(x0 + 2 * x1  <= 4)

    res = mdl.optimize()
    print(res)
