from numbers import Number

def listify(x, repeat=1):
    return x if isinstance(x, list) else [x] * repeat

def tuplelist(x):
    if isinstance(x, Number):
        return list(map(lambda x: (x,), range(x)))
    elif isinstance(x, tuple):
        return [x]
    elif (isinstance(x, list) or
          isinstance(x, set)  or
          isinstance(x, frozenset)):
        return list(
                map(lambda x: (x,) if isinstance(x, Number) else x, list(x)))
    raise 'tuplelist() unexpected argument type'


if __name__ == '__main__':

    print("\n==== Demo tuplelist() ===\n")

    arg = (5,)
    print("tuplelist(" + str(arg) + ") = " + str(tuplelist(arg)))
    print()

    arg = [(5,)]
    print("tuplelist(" + str(arg) + ") = " + str(tuplelist(arg)))
    print()

    arg = [(5,), (2,3)]
    print("tuplelist(" + str(arg) + ") = " + str(tuplelist(arg)))
    print()

    arg = [5, 2, 3]
    print("tuplelist(" + str(arg) + ") = " + str(tuplelist(arg)))
    print()

    arg = [5, (2, 1), 3]
    print("tuplelist(" + str(arg) + ") = " + str(tuplelist(arg)))
    print()

    arg = 3
    print("tuplelist(" + str(arg) + ") = " + str(tuplelist(arg)))
    print()
