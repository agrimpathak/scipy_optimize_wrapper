from collections import defaultdict
from functools   import reduce
from itertools   import combinations
from util        import *

# Dictionary that maps a tuple -> [value].  Allows for very flexibile
# pattern matching querying, e.g.  tupledict.select(1,'*', 5) or
# tupledict[(0, '*', 5)] will return a list of values with all associated
# keys with values 1 in 0th position and 5 in the 2nd position

# Limitations: Does not currently support overwriting or deleting keys

class tupledict:

    # Constructors #

    def __init__(self, kvargs=[]):
        self.__dict        = defaultdict(list)
        self.__unique_keys = set()
        keys = list(map(lambda kv: kv[0], kvargs))
        vals = list(map(lambda kv: kv[1], kvargs))
        self.add(keys, vals)

    # Public #

    # Note: tuple_keys containing the '*' will corrupt functionality
    def add(self, tuple_keys, vals):
        tuple_keys = listify(tuple_keys)
        vals       = listify(vals)
        self.__dict[()].extend(vals)
        for tuple_key, val in zip(tuple_keys, vals):
            if not isinstance(tuple_key, tuple):
                raise TypeError('Key must be a tuple')
            if tuple_key in self.__unique_keys:
                raise RuntimeError('Key already exists')
            self.__unique_keys.add(tuple_key)
            for sub_key_sz in range(1 + len(tuple_key)):
                for indices in combinations(range(len(tuple_key)), sub_key_sz):
                    sub_key_l = list(tuple_key)
                    for idx in indices:
                        sub_key_l[idx] = '*'
                    sub_key_t = tuple(sub_key_l)
                    self.__dict[sub_key_t].append(val)
        return self

    def select(self, *pattern):
        return self.__dict[tuple(i for i in pattern)]

    # Operators #

    def __getitem__(self, pattern):
        val = (self.__dict[pattern] if   isinstance(pattern, tuple)
                                    else self.__dict[(pattern,)])
        return val[0] if len(val) == 1 else val

    def __str__(self):
        s = ""
        for k,v in self.__dict.items():
            s += str(k) + " => " + str(v) + '\n'
        return s

    # Does not currently support patterns
    def __in__(self, key):
        return key in self.__unique_keys

    def __len__(self):
        return len(self.__unique_keys)

    def sum(self, *pattern):
        return reduce(lambda x, y: x + y, self.select(*pattern))


if __name__ == '__main__':

    d = tupledict([ (('a','b'), 1) ,
                    (('a','d'), 2) ,
                    (('e','f'), 3) ])

    print(d)
