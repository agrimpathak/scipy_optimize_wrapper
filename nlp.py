import numpy
from   scipy.optimize import minimize
from   tupledict      import *
from   util           import *

class NlpVar:

    # Constructors #

    def __init__(self, id, lb, ub):
        self.__id  = id
        self.__bnd = (lb, ub)

    # Operators

    def __call__(self, x):
        return x[self.__id]

    # Getters / Attributes #

    def id(self):
        return self.__id

    def bounds(self):
        return self.__bnd


class NlpModel:

    # Constructors #

    def __init__(self):
        self.__vars  = []
        self.__cons  = []
        self.__sense = 1.0

    # Public #

    def addVar(self, lb=None, ub=None):
        new_var = NlpVar(len(self.__vars), lb, ub)
        self.__vars.append(new_var)
        return new_var

    # indices can be any of the following:
    # 1) A number
    # 2) A list or set of numbers
    # 3) A tuple
    # 4) A list of tuples
    def addVars(self, indices, lb=None, ub=None):
        indices      = tuplelist(indices)
        n            = len(indices)
        s            = len(self.__vars)
        lb           = listify(lb, n)
        ub           = listify(ub, n)
        new_vars     = [ NlpVar(_id, _lb, _ub)
                         for (_id, _lb, _ub)
                         in zip(range(s, s+n), lb, ub) ]
        self.__vars += new_vars
        return tupledict().add(indices, new_vars)

    def setObjective(self, func):
        self.__obj = func

    def setSenseToMinimize(self):
        self.__sense = +1.0

    def setSenseToMaximize(self):
        self.__sense = -1.0

    def addConstr(self, func, eq_type):
        self.__cons.append({'type': eq_type                                  ,
                            'fun' : func                                     ,
                            'jac' : func.jac if 'jac' in dir(func) else None })

    def addEqualityConstr(self, func):
        self.addConstr(func, 'eq')

    def addInequalityConstr(self, func):
        self.addConstr(func, 'ineq')

    def optimize(self, x0, method='SLSQP', args=(), options={'disp' : True}):
        bnd   = list(map(lambda v: v.bounds(), self.__vars))
        has_j = 'jac' in dir(self.__obj)
        jac   = (lambda x, *args: self.__sense * self.__obj.jac(x, *args)
                    if has_j else None)
        return minimize(lambda x, *args: self.__sense * self.__obj(x, *args) ,
                        x0                                                   ,
                        bounds      = bnd                                    ,
                        jac         = jac                                    ,
                        constraints = self.__cons                            ,
                        method      = method                                 ,
                        args        = args                                   ,
                        options     = options                                )

    # Getters / Attributes #

    def getVars(self):
        return self.__vars

    def getConstraints(self):
        return self.__cons


# Test example and scipy.optimize.minimize documentation:
#
# Constrained minimization of multivariate scalar functions (minimize)
# https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html

if __name__ == '__main__':

    # Note: Parameters y and z are not used.  Only passed for illustration.
    class Func:

        def __call__(self, x, y, z):
            return 2*x[0]*x[1] + 2*x[0] - x[0]**2 - 2*x[1]**2

        def jac(self, x, y, z):
            dfdx0 = -2 * x[0] + 2 * x[1] + 2
            dfdx1 =  2 * x[0] - 4 * x[1]
            return numpy.array([dfdx0, dfdx1])


    class Cons1:

        def __call__(self, x):
            return numpy.array([x[0]**3 - x[1]])

        def jac(self, x):
            return numpy.array([3.0 * (x[0]**2.0), -1.0])


    class Cons2:

        def __call__(self, x):
            return numpy.array([x[1] - 1])

        def jac(self, x):
            return numpy.array([0.0, 1.0])


    mdl = NlpModel()
    mdl.setObjective(Func())
    mdl.setSenseToMaximize()

    x0   = [-1.0, 1.0]
    yz   = (5, 2)
    meth = 'SLSQP'
    opt  = { 'disp' : True }

    print("\n=== Unconstrained ===\n")
    print(mdl.optimize(x0, method=meth, options=opt, args=yz))

    print("\n=== Constrained ===\n")
    mdl.addEqualityConstr(Cons1())
    mdl.addInequalityConstr(Cons2())
    print(mdl.optimize(x0, method=meth, options=opt, args=yz))
